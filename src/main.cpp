#include <iostream>
#include <fstream>
#include "leitordearq.hpp"
#include "imagempgm.hpp"
#include "imagemppm.hpp"

using namespace std;

int main(int argc, char ** argv){
	
	int escolha;
	
	cout << "\n\tDESCRIPTOGRAFAR IMAGEM PPM/PGM:\n";
	cout << "\n\t1 - Imagem PGM\n\t2 - Imagem PPM\n\tEscolha uma opção: ";
	cin >> escolha;
	cout << endl; 
	
	if (escolha==2){
		ImagemPPM arq1;
		arq1.Mensagem();
	}
	else if(escolha==1){
		ImagemPGM arq;
		arq.Mensagem();
	}
	else{
		cout << "\tOpção invalida.\n";
		exit(0);
	}
	
	return 0;
}
