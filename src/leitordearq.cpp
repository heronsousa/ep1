#include <iostream>
#include <fstream>
#include <string.h>
#include "leitordearq.hpp"

using namespace std;

Arquivo::Arquivo(){
	cout << "\tDigite o nome da imagem ('nome_da_imagem.extensão'. Exemplo: exemplo.pgm): ";
	scanf(" %[^\n]s", diretorio);
	cout << endl; 
}

Arquivo::~Arquivo(){ }

void Arquivo::setLinhas(int linhas){
	this->linhas = linhas;
}
int Arquivo::getLinhas(){
	return linhas;
}
void Arquivo::setColunas(int colunas){
	this->colunas = colunas;
}
int Arquivo::getColunas(){
	return colunas;
}
void Arquivo::setPosicao(int pos){
	this->pos = pos;
}
int Arquivo::getPosicao(){
	return pos;
}
void Arquivo::setTamanho(int tamanho){
	this->tamanho = tamanho;
}
int Arquivo::getTamanho(){
	return tamanho;
}
void Arquivo::setnCifra(int nCifra){
	this->nCifra = nCifra;
}
int Arquivo::getnCifra(){
	return nCifra;
}
void Arquivo::setPosSeek(int posSeek){
	this->posSeek = posSeek;
}
int Arquivo::getPosSeek(){
	return posSeek;
}

void Arquivo::Imagem(){
	
	ifstream arquivo;
	arquivo.open(diretorio);
	if(arquivo){
//------------------------------

//Do arquivo(imagem) atribui-se os valores, em sequência, às variaveis	
	arquivo >> tipo >> posicao >> tamanho >> cifra >> linhas >> colunas >> tons;
	
	posSeek = arquivo.tellg(); //Numero da posição que será usado na função "fseek"

//Remoção do caracetere '#' da variavel 'posicao':
	int i, n, j;
	
	n = strlen(posicao);
	for(i=0, j=0; i<n; i++){
		if(posicao[i] != '#'){
			posicao[j] = posicao[i];
			j++;
		}
	}
	posicao[j] = '\0';
//------------------------------------------
	}
	else {
		cout << "\tNão foi possivel abrir a imagem!\n\n";
		exit(0);
	}
	pos = atoi(posicao); //Transforma em inteiro, a variavel "posicao e cifra", que é do tipo char
	nCifra = atoi(cifra);
	arquivo.close(); //Fecha o arquivo
	
}
 	








