#ifndef IMAGEMPGM_HPP
#define IMAGEMPGM_HPP

#include <iostream>
#include "leitordearq.hpp"

class ImagemPGM : public Arquivo{

	private:
		char mensagem; 
		int cont, valor, x, z, nValor, nCifra;

	public:
		ImagemPGM();
		~ImagemPGM();

		void Mensagem();
};

#endif
