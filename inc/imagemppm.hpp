#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP

#include <iostream>
#include "leitordearq.hpp"

using namespace std;

class ImagemPPM : public Arquivo{

	private:
		int valor, cont;
		string tipo, temp= "", chave;
		
	public: 
		ImagemPPM();
		~ImagemPPM();
		
		void Mensagem();

};

#endif
