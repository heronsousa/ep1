#ifndef LEITORDEARQ_HPP
#define LEITORDEARQ_HPP
#include <iostream>
#include <string>

using namespace std;

class Arquivo{

	private:
		int posSeek, tamanho, nCifra, pos, escolha, linhas, colunas;
		char posicao[20];
		string  tipo, tons;
	
	protected:
		char diretorio[30], cifra[10];
	
	public:		
		
		Arquivo();
		Arquivo(char diretorio[30]);
		~Arquivo();

		void abrirImagem();
		void Imagem();
		void setPosicao(int pos);
		int getPosicao();
		void setLinhas(int linhas);
		int getLinhas();
		void setColunas(int colunas);
		int getColunas();
		void setTamanho(int tamanho);
		int getTamanho();
		void setnCifra(int cifra);
		int getnCifra();
		void setPosSeek(int posSeek);
		int getPosSeek();
};	

#endif
